# ign-launch-release

The ign-launch-release repository has moved to: https://github.com/ignition-release/ign-launch-release

Until May 31st 2020, the mercurial repository can be found at: https://bitbucket.org/osrf-migrated/ign-launch-release
